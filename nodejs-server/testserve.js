const express = require('express');
const cors = require('cors');
const https = require('https');
const fs = require('fs');
const app = express();
const port = 5829;

const options = {
	key : fs.readFileSync('/home/sermuns/.ssl/key.pem'),
	cert: fs.readFileSync('/home/sermuns/.ssl/cert.pem')
};

app.use(cors());

app.get('/:message', (req, res) => {
	  const message = req.params.message;
	  res.send(message);
});

https.createServer(options, app).listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
