const express = require('express');
const bodyParser = require('body-parser');
const { spawn } = require('child_process');
const fs = require('fs');
const https = require('https')
const cors = require('cors')

const app = express();
const port = 5829;

app.use(cors())
app.use(bodyParser.json());

// Endpoint to handle queries
app.get('/:query', (req, res) => {
  const query = req.params.query;
	console.log("Incoming query for:", query)

  // Check if the query exists in the data file (data.json)
  fs.readFile('data.json', 'utf8', (err, data) => {
    if (err) {
      console.error(err);
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }

    const dataObj = JSON.parse(data);
    if (dataObj.hasOwnProperty(query)) {
	    console.log("Found in database! Responding..")
      // Query found in data.json, return the value
      res.json({ result: dataObj[query] });
    } else {
      // Query not found, run the Python script
	    console.log("Not in database! Scraping...")
      const pythonProcess = spawn('python3', ['urlgetter.py', query]);

      // Capture the output of the Python script
      pythonProcess.stdout.on('data', (output) => {
        const result = output.toString().trim();
	      if(result == '-1'){
		      console.log("Invalid query:",query)
	      }

        // Store the result in data.json with the query as the key
        dataObj[query] = result;
        fs.writeFile('data.json', JSON.stringify(dataObj), 'utf8', (err) => {
          if (err) {
            console.error(err);
          }
        });

        // Return the result to the client
        res.json({ result });
	      console.log("Response scraped and sent. Added to database.")
	      
      });

      pythonProcess.stderr.on('data', (error) => {
        console.error(error.toString());
        res.status(500).json({ error: 'Internal Server Error' });
      });
    }
  });
});

const glx = require('greenlock-express').init({
        packageRoot: __dirname,
        // where to look for configuration
        configDir: './greenlock.d',
        // whether or not to run at cloudscale
        cluster: false
})

glx.serve(app)
