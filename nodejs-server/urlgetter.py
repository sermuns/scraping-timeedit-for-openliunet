import sys
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC

### 0. SETUP

args = sys.argv
query = str(args[1])
if(len(query) < 2):
    print('-1')
    sys.exit()

# Define the URL
init_url = "https://cloud.timeedit.net/liu/web/schema/"

# Start headless webdriver!
options = webdriver.ChromeOptions()
options.add_argument('--headless')
driver = webdriver.Chrome(options=options)

def getUrl(today : bool) -> str:
    # Send an HTTP GET request to the URL using Selenium
    driver.get(init_url)
    wait = WebDriverWait(driver, 5)  # Adjust the timeout as needed


    ### 1. ENTER SCHEMA SÖK

    # Find and click the element with the selector "h2.greenlink"
    greenlink_element = driver.find_element(By.CSS_SELECTOR, "h2.greenlink")
    greenlink_element.click()

    # Wait for the URL to change (indicating the redirection)
    wait.until(EC.url_changes(init_url))

    ## 2. SEARCH FOR COURSE ON ri1Q7.html
    # Change the date from (start of termin) to (yesterday)

    if(not(today)):
        # Open start range popup
        startRangeButton = driver.find_element(By.ID, "openStartRangeButton")
        startRangeButton.click()

        ## start range
        # Pick august
        monthPicker = wait.until(EC.presence_of_element_located((By.CLASS_NAME, "ui-datepicker-month")))
        Select(monthPicker).select_by_visible_text("Augusti")

        # Click the 1st of month
        day_elements = wait.until(EC.presence_of_all_elements_located((By.CLASS_NAME, "ui-state-default")))
        day_elements[2].click()

    # Locate the select element by its "id" attribute
    select_element = Select(driver.find_element(By.ID, "fancytypeselector"))

    # Select the option with textContent "Kurs"
    select_element.select_by_visible_text("Kurs")

    # Input course name in search
    input_box = driver.find_element(By.ID, "ffsearchname")
    input_box.send_keys(query)

    # Click "Sök"
    driver.find_element(By.CLASS_NAME, "ffsearchbutton").click()

    try:
        # Click "Lägg till alla"
        addallbutton = wait.until(EC.presence_of_element_located((By.CLASS_NAME, "addallbutton")))
        addallbutton.click()
    except Exception as e:
        return('-1')

    # Click "Visa Schema"
    url = driver.current_url
    showScheduleButton = wait.until(EC.presence_of_element_located((By.ID, "objectbasketgo")))
    showScheduleButton.click()
    wait.until(EC.url_changes(url))
    return(driver.current_url)

totalUrl = getUrl(False)
if(totalUrl == '-1'):
    print('-1 -1')
else:
    todayUrl = getUrl(True)
    print(totalUrl, todayUrl)

### 3. Count the occurences of future 'Föreläsning'
## Find all <td> elements with class "column1"
#td_elements = wait.until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, 'td.column1')))
#
## Initialize a count variable
#future_count = 0
#
## Loop through the found <td> elements and count occurrences of "Föreläsning"
#for td in td_elements:
#    if td.text.strip() == "Föreläsning":
#        future_count += 1
#
### 4. Change the date from (start of termin) to (yesterday)
#
## Open start range popup
#startRangeButton = driver.find_element(By.ID, "openStartRangeButton")
#startRangeButton.click()
#
### start range
## Pick august
#monthPicker = wait.until(EC.presence_of_element_located((By.CLASS_NAME, "ui-datepicker-month")))
#Select(monthPicker).select_by_visible_text("Augusti")
#
## Click the 1st of month
#day_elements = wait.until(EC.presence_of_all_elements_located((By.CLASS_NAME, "ui-state-default")))
#day_elements[1].click()
#
#td_elements = wait.until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, 'td.column1')))
#
## Initialize a count variable
#total_count = 0
#
## Loop through the found <td> elements and count occurrences of "Föreläsning"
#for td in td_elements:
#    if td.text.strip() == "Föreläsning":
#        total_count += 1
#
#print(total_count-future_count, "/", total_count)

# Close the WebDriver
driver.quit()
